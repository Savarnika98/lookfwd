import React, { Component } from 'react';
import Routes from './routes'

class App extends Component {

  constructor(props){
     super(props);
     this.state={
     appName: "Banana Project"
    }
   }

  render() {
  return (
    <div className="off-canvas-wrapper">
    <div className="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <div className="off-canvas-content" data-off-canvas-content>
    <Routes name = {this.state.appName}/>
    </div>
    </div>
    </div>
 );
}
}

export default App;
