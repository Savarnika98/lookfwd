import React from 'react';

import { Route,Redirect,Switch,BrowserRouter as Router} from 'react-router-dom';

import Welcome from '././components/Welcome/Welcome';
import Home from '././components/Home/Home';

const Routes = () => (
    <Router>
     <Route exact path = "/" component = {Welcome}/>
     <Route path = "/home" component = {Home}/> 
     </Router>
    
)

export default Routes;
