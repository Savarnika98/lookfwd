import React, {Component} from 'react';
import FacebookLogin from 'react-facebook-login';
import GoogleLogin from 'react-google-login';
import { Redirect } from 'react-router';
import {PostData} from '../../services/PostData'

class Welcome extends Component
 {
    constructor(props) {
        super(props);
        this.state = {
            loginError : false,
            redirect : false
        }
        this.signup = this.signup.bind(this)
 }
 signup(res,type) {
     let postData;
     if(type === 'facebook' && res.email) {
         postData = {
             name: res.name,
             provider : type,
             email : res.email,
             provider_id : res.id,
             token : res.accessToken,
             provider_pic : res.picture.data.url
         }
     }

     if(postData) {
        PostData('signup', postData).then((result) => {
            let responseJson = result;
            sessionStorage.setItem("userData", JSON.stringify(responseJson));
            this.setState({redirect : true})
        });
       
    } else {
        console.log("No postdata")
    }
 }
    render() {
        if(this.state.redirect || sessionStorage.getItem('userData')) {
            return (<Redirect to = {'/home'}/>)
        }

        const responseFacebook = (response) => {
            console.log("facebook console");
            console.log(response);
            this.signup(response, 'facebook');
        }
        
        const responseGoogle = (response) => {
            console.log("google console");
            console.log(response);
            this.signup(response, 'google');
        }

    return(
        <div className= "row" id = "Body">
        <div className = "medium-12 columns">
            <h2> Welcome page </h2>

            <FacebookLogin
appId="3109980755899522"
autoLoad={false}
fields="name,email,picture"
callback={responseFacebook}/>
<br/><br/>

{/*<GoogleLogin
clientId="Your Google ID"
buttonText="Login with Google"
onSuccess={responseGoogle}
onFailure={responseGoogle}/> */}
          
        </div>    
        </div>
    )
    }
}

export default Welcome;